import React, { useState, useEffect } from 'react'
import classnames from 'classnames'
import './App.css'

/*
  Utility functions
*/
function byListIdThenName(a, b) {
  if (a.listId === b.listId) {
    // Note: this sort would be a business decision
    // I chose to sort numerically because of the data format
    // it looks much better than the string sort would have
    const [aPre, aNum] = a.name.split('Item ')
    const [bPre, bNum] = b.name.split('Item ')
    return +aNum < +bNum ? -1 : 1
  }
  return a.listId < b.listId ? -1 : 1
}

const sort = (data) => data.sort(byListIdThenName)
// this filters out any 'falsey' name including null, undefined, empty string and 0
const filter = (data) => data.filter(({ name }) => !!name)
// in production code I would use a library with utility functions such as lodash's groupBy
const group = (data) => {
  const groups = {}
  data.forEach((d) => {
    groups[d.listId] = groups[d.listId] || []
    groups[d.listId].push(d)
  })
  return groups
}

/*
  Service/DataLayer functions
*/
function getData() {
  return fetch('https://fetch-hiring.s3.amazonaws.com/hiring.json')
    .then((response) => response.json())
    .then(filter)
    .then(sort)
    .then(group)
}

/*
  Functional Components
*/
function ListItem({ name, listId, id }) {
  return (
    <div data-test-id={id} className="list-item">
      <span>{name}</span>
    </div>
  )
}

function List({ title, values = [], id, collapsed, onCollapse }) {
  return (
    <div
      data-test-id={id}
      key={title}
      className={classnames('list-items', { collapsed })}
    >
      <h3 onClick={onCollapse}>
        {title} <i className="fas fa-caret-up" />
      </h3>
      <span className="item-count-label">{values.length} items</span>
      <div>
        {values.map((v) => (
          <ListItem key={v.id} {...v} />
        ))}
      </div>
    </div>
  )
}

/*
  Main Application
*/

function App() {
  const [data, setData] = useState([])
  const [loading, setLoading] = useState(true)
  const [collapsedPanels, setCollapsedPanel] = useState({})

  const fetchData = () => {
    getData().then((groupedData) => {
      setData(groupedData)
      Object.keys(groupedData).forEach((key) => {
        collapsedPanels[key] = true
      })
      setCollapsedPanel(collapsedPanels)
      setLoading(false)
    })
  }

  const handleCollapsePanel = (panelId) => {
    collapsedPanels[panelId] = !collapsedPanels[panelId]
    setCollapsedPanel({ ...collapsedPanels })
  }

  const allOpen = () => {
    return !Object.values(collapsedPanels).filter((o) => !!o).length
  }

  const toggleAllPanels = (panelId) => {
    const open = allOpen()
    Object.keys(collapsedPanels).forEach((key) => {
      collapsedPanels[key] = open
    })
    setCollapsedPanel({ ...collapsedPanels })
  }

  useEffect(fetchData, [])

  return (
    <div className="app">
      <div className="side-panel">
        <header>
          <img height={128} src="fetch.png" />
        </header>
        <aside>
          <a
            href="https://www.linkedin.com/in/jason-baddley-36167931/"
            target="blank"
          >
            <i className="fab spin fa-linkedin" />
            <span>by Jason Baddley</span>
          </a>
        </aside>
      </div>
      <div className={classnames('content', { loading })}>
        <div className="loader">
          <i className="fas fa-spin fa-spinner" /> Loading Data
        </div>
        <h3>Click on a list to toggle its list of items</h3>
        <a
          className={classnames('toggle-panels', { collapsed: !allOpen() })}
          onClick={toggleAllPanels}
        >
          {allOpen() ? 'Close' : 'Open'} All <i className="fas fa-caret-up" />
        </a>
        {Object.entries(data).map(([listId, values]) => (
          <List
            collapsed={collapsedPanels[listId]}
            onCollapse={() => handleCollapsePanel(listId)}
            key={listId}
            id={listId}
            title={`List ${listId}`}
            values={values}
          />
        ))}
      </div>
    </div>
  )
}

export default App
