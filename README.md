# Fetch Rewards Coding Exercise

### By Jason Baddley

## Instructions

1. Clone the repo which was bootstrapped using `create-react-app`
2. run `npm start` or `yarn start` at the root of the project
3. Go to `localhost:3000` to see the results

Note: All applicable code is contained in `App.js` and `App.css`. All other code is part of create react app.
